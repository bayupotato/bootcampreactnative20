//Soal 1
var string1 = "Javascript"
var string2 = "is"
var string3 = "awesome"
var string4 = "and i"
var string5 = "love"
var string6 = "it"

//Penyelesaian
console.log(string1.concat(" ",string2," ",string3, " ",string4, " ",string5, " ",string6," "))

//Soal 2
var sentence= "I am going to be React Native Developer"
var FirstWord = sentence[0]
var SecondWord = sentence[2]+sentence[3]
var ThirdWord = sentence[4]+sentence[5]+sentence[6]+sentence[7]+sentence[8]+sentence[9]
var FourthWord = sentence[11]+sentence[12]
var FifthWord = sentence[14]+sentence[15]
var SixthWord = sentence[16]+sentence[17]+sentence[18]+sentence[19]+sentence[20]+sentence[21]
var SeventhWord = sentence[23]+sentence[24]+sentence[25]+sentence[26]+sentence[27]+sentence[28]
var EighthWord = sentence[29]+sentence[30]+sentence[31]+sentence[32]+sentence[33]+sentence[34]+sentence[35]+sentence[36]+sentence[37]+sentence[38]
//penyelesaian

console.log('First Word',":"+ FirstWord)
console.log('Second Word',":"+ SecondWord)
console.log('Third Word',":"+ ThirdWord)
console.log('Fourth Word',":"+ FourthWord)
console.log('Fifth Word',":"+ FifthWord)
console.log('Sixth Word',":"+ SixthWord)
console.log('Seventh Word',":"+ SeventhWord)
console.log('Eighth Word',":"+ EighthWord)

//soal 3

var sentence2 = "wow JavaScript is so cool"
var FirstWord2 = sentence2.substring(0, 3)
var SecondWord2 = sentence2.substring(4,14)
var ThirdWord2 = sentence2.substring (15, 17)
var FourthWord2 = sentence2.substring (18, 20)
var FifthWord2 = sentence2.substring (21, 25)
//penyelesaian
console.log("First Word",":"+ FirstWord2)
console.log("Second Word",":"+ SecondWord2)
console.log("Third Word",":"+ ThirdWord2)
console.log("Fourth Word",":"+ FourthWord2)
console.log("Fifth Word",":"+ FifthWord2)

//soal 4
var sentence3 = "wow JavaScript is so cool"
var FirstWord3 = sentence3.substring(0, 3)
var SecondWord3 = sentence3.substring(4,14)
var ThirdWord3 = sentence3.substring (15, 17)
var FourthWord3 = sentence3.substring (18, 20)
var FifthWord3 = sentence3.substring (21, 25)
var FirstWordLength = FirstWord3.length
var SecondWordLength = SecondWord3.length
var ThirdWordLength = ThirdWord3.length
var FourthWordLength = FourthWord3.length
var FifthWordLength = FifthWord3.length
//penyelesaian
console.log("First Word",":"+ FirstWord3+" ","with length",":"+FirstWordLength)
console.log("Second Word",":"+ SecondWord3+" ","with length",":"+SecondWordLength)
console.log("Third Word",":"+ ThirdWord3+" ","with length",":"+ThirdWordLength)
console.log("Fourth Word",":"+ FourthWord3+" ","with length",":"+FourthWordLength)
console.log("Fifth Word",":"+ FifthWord3+" ","with length",":"+FifthWordLength)