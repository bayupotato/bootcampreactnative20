//No.1 While Loop
var judul1 = "LOOPING PERTAMA"
console.log(judul1)
var deret = "2"
var jumlah = "22"
while(deret<jumlah) {
    console.log(deret,"-"+" ","i love coding")
    if(deret == jumlah) {
        deret = false   
    }
    deret++
    deret++
}
var judul1 = "LOOPING KEDUA"
console.log(judul1)
var deret = "20"
var jumlah = "0"
while(deret>jumlah) {
    console.log(deret,"-"+" ","i love coding")
    if(deret == jumlah) {
        deret = false   
    }
    deret--
    deret--
}

//No.2 For Loop
for(let angka = 1; angka < 21; angka++) {
    if(angka %3 == 0) {
        console.log(angka+" ","I love coding")
    }
    else if(angka %2 ==0) {
        console.log(angka+" ","Berkualitas")
    }
    else if(angka %2 !==0) {
        console.log(angka+" ","Santai")
    }
}

//No.3 Membuat Persegi panjang
for(let index = 0; index < 4;index++) {
    console.log("########")
}
  
//No.4 Membuat Segitiga
var jumlah = 0
let pagar = "#"
for(var angka = 1; angka < 8; angka++) {
    console.log(pagar)
    pagar += "#"
}

//No.5 Membuat Papan Catur
let hasil = ""
for(let index = 1; index < 71; index++){
    if (index % 9 == 0) {
        hasil += "\n"
    } else if (index % 2 !==0) {
        hasil += " "
        
    } else if (index % 2 == 0) {
        hasil += "#"
    }   
}
console.log(hasil)